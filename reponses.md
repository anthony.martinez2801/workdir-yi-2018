# TP Realworld
Créer un nouveau paragraphe pour chaque section 6.x.y

## 6.3
### 6.3.1
7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7

### 6.3.2
https://github.com/mkenney/docker-npm/blob/master/node-8-debian/Dockerfile

Il y a 1 volume déclaré : VOLUME /src

docker image pull mkenney/npm:node-8-debian

Image ID : 
9af19a7f4e2c

### 6.3.3
docker container run -ti --rm -v $(pwd):/src mkenney/npm:node-8-debian npm install

### 6.3.4
docker container run -ti --rm -v $(pwd):/src -p 8000:8080 mkenney/npm:node-8-debian npm run dev

## 6.4
### 6.4.1
eef3d052fe83dc688c0eb3dab97876d714a2d14d

### 6.4.2
docker image pull gradle:4.7.0-jdk8-alpine

Image ID : f438b7d58d0a

Il y a 1 volume déclaré : /home/gradle/.gradle

### 6.4.3
docker volume create gradle-home

docker volume ls 

DRIVER              VOLUME NAME
local               8f2d28f1a61f276beb02e3062b6a6a9500e11f02b7eef0ca1a85f11725e3c4b3
local               gradle-home
local               ls
local               nginx-logs

### 6.4.4
docker container run -ti --rm -v $(pwd):/src -v gradle-home:/home/gradle/.gradle -w /src gradle:4.7.0-jdk8-alpine gradle build

### 6.4.5
docker container run -ti --rm -v $(pwd):/src -v gradle-home:/home/gradle/.gradle -w /src -p 8001:8080 gradle:4.7.0-jdk8-alpine gradle bootRun

## 6.5
### 6.5.1
{"articles":[{"id":"b4f6434d-521b-4474-9ad4-094ca429b7fb","slug":"hello","title":"Hello","description":"Someone or something","body":"## Oui\nMy story is... a story","favorited":false,"favoritesCount":0,"createdAt":"2018-05-16T12:20:50.183Z","updatedAt":"2018-05-16T12:20:50.183Z","tagList":[],"author":{"username":"Pitysha","bio":"","image":"https://static.productionready.io/images/smiley-cyrus.jpg","following":false}}],"articlesCount":1}

## 6.6
### 6.6.1
docker container run -ti --rm -v $(pwd):/src mkenney/npm:node-8-debian npm run build

ls dist

favicons-c2a605fbc0e687b2e1b4b90a7c445cdd  index.html  static

### 6.6.2
docker container run -ti --rm -v $(pwd)/dist:/usr/share/nginx/html -p 8000:80 nginx:alpine

L'article créé précédemment est bien présent. 

### 6.6.3






